//
//  CarResponse.swift
//  SwiftUIAndUIKit
//
//  Created by Viktoriia Skvarko on 16.08.2021.
//

import Foundation
import UIKit
import SwiftUI

struct CarResponse: Hashable, Codable, Identifiable {
    var id: Int
    
    var imageName: String
    var brendAndModel: String
    var description: String
}
