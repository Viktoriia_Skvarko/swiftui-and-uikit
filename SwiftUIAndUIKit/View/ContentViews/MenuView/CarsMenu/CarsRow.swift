//
//  CarsRow.swift
//  SwiftUIAndUIKit
//
//  Created by Viktoriia Skvarko on 16.08.2021.
//

import SwiftUI
import UIKit

struct CarsRow: View {
    var items: [CarResponse]
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            
            ScrollView(showsIndicators: false) {
                HStack(alignment: .center, spacing: 0) {
                    
                    ForEach(self.items, id: \.self) { mode in
                        HStack{
                            CarsItem(object: carResponse[0])
                            CarsItem(object: carResponse[1])
                            CarsItem(object: carResponse[2])
                            CarsItem(object: carResponse[3])
                            CarsItem(object: carResponse[4])
                        }
                    }
                }
            }.frame(height: 190)
        }
    }
}

struct CarsRow_Previews: PreviewProvider {
    static var previews: some View {
        CarsRow(items: Array(carResponse.prefix(3)))
    }
}
