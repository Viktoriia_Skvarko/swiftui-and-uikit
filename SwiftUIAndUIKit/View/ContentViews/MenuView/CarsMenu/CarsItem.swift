//
//  CarsItem.swift
//  SwiftUIAndUIKit
//
//  Created by Viktoriia Skvarko on 16.08.2021.
//

import SwiftUI

struct CarsItem : View {
    
    var object: CarResponse
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10.0) {
            Image(object.imageName)
                .resizable()
                .renderingMode(.original)
                .cornerRadius(10)
                .frame(width: 170, height: 170)
        }.padding(.leading, 15)
    }
}

struct CarsItem_Previews: PreviewProvider {
    static var previews: some View {
        CarsItem(object: carResponse[0])
    }
}
