//
//  MenuView.swift
//  SwiftUIAndUIKit
//
//  Created by Viktoriia Skvarko on 16.08.2021.
//

import SwiftUI
import UIKit

struct MenuView: View {
    
    var body: some View {
        
        NavigationView {
            VStack(alignment: .leading, spacing: 10) {
                Image("socialImg")
                    .resizable()
                    .frame(width: 380, height: 250, alignment: .center)
                    .cornerRadius(20)
                    .padding()
                
                VStack {
                    HStack {
                        ScrollView(.horizontal, showsIndicators: false) {
                            ForEach(0..<1, id: \.self) { items in
                                CarsRow(items: carResponse)
                            }.listRowInsets(EdgeInsets())
                        }
                    }
                    
                    HStack {
                        ScrollView(.horizontal, showsIndicators: false) {
                            ForEach(0..<1, id: \.self) { items in
                                CarsRow(items: carResponse)
                            }.listRowInsets(EdgeInsets())
                            Spacer()
                        }
                    }
                }
                Spacer()
            }
        }.navigationBarTitle(Text("Interesting"))
    }
}

struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
        MenuView()
    }
}
