//
//  CellMenu.swift
//  SwiftUIAndUIKit
//
//  Created by Viktoriia Skvarko on 16.08.2021.
//

import SwiftUI
import UIKit

struct CellMenu: View {
    
    var car: CarResponse
    
    var body: some View {
        Image(car.imageName)
            .resizable()
            .frame(width: 150, height: 150)
            .cornerRadius(15)
    }
}

struct CellCarsList_Previews: PreviewProvider {
    static var previews: some View {
        CellMenu(car: carResponse[0])
    }
}
