//
//  ViewController.swift
//  SwiftUIAndUIKit
//
//  Created by Viktoriia Skvarko on 16.08.2021.
//

import UIKit
import SwiftUI

class ViewController: UIViewController {
    
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var startButton: UIButton!
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeElement()
    }
    
    
    func customizeElement() {
        startButton.backgroundColor = UIColor.systemRed
        startButton.setTitleColor(UIColor.white, for: .normal)
        startButton.layer.cornerRadius = 10
        startButton.layer.borderWidth = 0.2
        startButton.layer.shadowOpacity = 0.7
        startButton.layer.shadowColor = UIColor.systemGray.cgColor
        startButton.layer.shadowRadius = 5
    }

    
    // MARK: - Actions
    
    @IBAction func tapStartButton(_ sender: Any) {
        let detailScreen = MenuView()
        let host = UIHostingController(rootView: detailScreen)
        navigationController?.pushViewController(host, animated: true)
    }
    
}

